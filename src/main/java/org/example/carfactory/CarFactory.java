package org.example.carfactory;

import java.util.*;

public class CarFactory {
    VehicleRegistrationNumberGenerator vehicleRegistrationNumberGenerator;
    String brand;
    Map<String,Model> models = new HashMap<>();
    Map<String,CarPackage> carPackages = new HashMap<>();
    Map<String,Equipment> equipments = new HashMap<>();

    public CarFactory(VehicleRegistrationNumberGenerator vehicleRegistrationNumberGenerator, String brand) {
        this.vehicleRegistrationNumberGenerator = vehicleRegistrationNumberGenerator;
        this.brand = brand;
    }

    public Car createNewCar(String modelName, String color, List<String> equipment, List<String> packages) throws MissingModelException, MissingPackageException, IllegalModelAndPackageCombinationException, IllegalCombinationOfEquipmentException {
        Model model = getModelByName(modelName);

        model.verifyCompatiblePackages(packages);

        return new Car(color,
                this.brand,
                vehicleRegistrationNumberGenerator.getNextRegNo(),
                model.getEngineType(),
                model.getEnginePower(),
                model.getNumberOfPassengers(),
                combineEquipmentAndCheckForDuplicates(equipment, packages, model),
                packages,
                calculatePrice(equipment, packages, model));
    }

    private double calculatePrice(List<String> equipment, List<String> packages, Model model) {
        return model.getPrice()
                + calculateEquipmentPrice(equipment)
                + calculateEquipmentPrice(model.getEquipment())
                + calculatePackagePrice(packages);
    }

    private List<String> combineEquipmentAndCheckForDuplicates(List<String> equipment, List<String> packages, Model model) throws MissingPackageException, IllegalCombinationOfEquipmentException {
        List<String> combinedEquipment = new ArrayList<>(equipment);
        combinedEquipment.addAll(model.getEquipment());
        appendPackageEquipment(packages, combinedEquipment);

        Set<String> equipmentDuplicates = findDuplicates(combinedEquipment);
        if (!equipmentDuplicates.isEmpty())
            throw new IllegalCombinationOfEquipmentException(String.join(",", equipmentDuplicates));

        return combinedEquipment;
    }

    private Model getModelByName(String modelAsText) throws MissingModelException {
        Model model = models.get(modelAsText);
        if (model==null)
            throw new MissingModelException(modelAsText);
        return model;
    }

    private double calculateEquipmentPrice(List<String> combinedEquipment) {
        return combinedEquipment.stream()
                .map(equ -> equipments.getOrDefault(equ, new Equipment(null,0D)))
                .mapToDouble(Equipment::getPrice)
                .sum();
    }

    private double calculatePackagePrice(List<String> carPackages) {
        return carPackages.stream()
                .map(equ -> this.carPackages.getOrDefault(equ, new CarPackage(null,List.of(), null, 0D)))
                .mapToDouble(CarPackage::getPrice)
                .sum();
    }

    private void appendPackageEquipment(List<String> packages, List<String> combinedEquipment) throws MissingPackageException {
        for (String carPackageName : packages) {
            CarPackage carPackage = carPackages.get(carPackageName);
            if (carPackage == null)
                throw new MissingPackageException(carPackageName);
            combinedEquipment.addAll(carPackage.getEquipment());
            if (carPackage.getInheritFromPackageName() != null)
                appendPackageEquipment(List.of(carPackage.getInheritFromPackageName()), combinedEquipment);
        }
    }

    public void addModel(String model, String engineType, int enginePower, int numberOfPassengers, List<String> equipment, List<String> compatiblePackages, double price) {
        models.put(model, new Model(model, engineType, enginePower, numberOfPassengers, equipment, compatiblePackages, price));
    }

    public void addPackage(String packageName, List<String> equipment, String inheritFromPackageName, double price) {
        carPackages.put(packageName, new CarPackage(packageName, equipment, inheritFromPackageName, price));
    }

    static Set<String> findDuplicates(List<String> listContainingDuplicates) {
        final Set<String> setToReturn = new HashSet<>();
        final Set<String> set1 = new HashSet<>();

        for (String yourInt : listContainingDuplicates) {
            if (!set1.add(yourInt)) {
                setToReturn.add(yourInt);
            }
        }
        return setToReturn;
    }

    public void addEquipment(String equipment, double price) {
        equipments.put(equipment, new Equipment(equipment, price));
    }

    static class Model {
        String model;
        String engineType;
        int enginePower;
        int numberOfPassengers;
        List<String> equipment;
        List<String> compatiblePackages;
        double price;

        public Model(String model, String engineType, int enginePower, int numberOfPassengers, List<String> equipment, List<String> compatiblePackages, double price) {
            this.model = model;
            this.engineType = engineType;
            this.enginePower = enginePower;
            this.numberOfPassengers = numberOfPassengers;
            this.equipment = equipment;
            this.compatiblePackages = compatiblePackages;
            this.price = price;
        }

        public String getModel() {
            return model;
        }

        public String getEngineType() {
            return engineType;
        }

        public int getEnginePower() {
            return enginePower;
        }

        public int getNumberOfPassengers() {
            return numberOfPassengers;
        }

        public List<String> getEquipment() {
            return equipment;
        }

        public List<String> getCompatiblePackages() {
            return compatiblePackages;
        }

        public double getPrice() {
            return price;
        }

        public void verifyCompatiblePackages(List<String> packages) throws IllegalModelAndPackageCombinationException {
            if (!getCompatiblePackages().containsAll(packages))
                throw new IllegalModelAndPackageCombinationException(String.join(",", packages));
        }
    }

    static class CarPackage {
        private String name;
        private List<String> equipment;
        private String inheritFromPackageName;
        private double price;

        public CarPackage(String name, List<String> equipment, String inheritFromPackageName, double price) {
            this.name = name;
            this.equipment = equipment;
            this.inheritFromPackageName = inheritFromPackageName;
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public List<String> getEquipment() {
            return equipment;
        }

        public String getInheritFromPackageName() {
            return inheritFromPackageName;
        }

        public double getPrice() {
            return price;
        }
    }

    static class Equipment {
        String name;
        double price;

        public Equipment(String name, double price) {
            this.name = name;
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }
    }
}
