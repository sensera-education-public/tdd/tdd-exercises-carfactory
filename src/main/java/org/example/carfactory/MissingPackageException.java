package org.example.carfactory;

public class MissingPackageException extends Exception {
    public MissingPackageException(String message) {
        super(message);
    }
}
