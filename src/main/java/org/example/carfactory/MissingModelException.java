package org.example.carfactory;

public class MissingModelException extends Exception {
    public MissingModelException(String message) {
        super(message);
    }
}
