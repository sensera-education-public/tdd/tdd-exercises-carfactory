package org.example.carfactory;

public class IllegalCombinationOfEquipmentException extends Exception {
    public IllegalCombinationOfEquipmentException(String message) {
        super(message);
    }
}
