# TDD Exercise CarFactory

Creating a car factory with TDD methology

## Commit

All parts of the exercises are documented in separate commits. 

## Prerequisites

[How to start GitLab.com projekt](https://link_to_start_gilab_projekt_video_instruktion)

[How to start Intellij Maven projekt projekt](https://link_to_start_intellij_maven_projekt_video_instruktion)

